#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>

void deleteSpaces(char string[]) {
	char* result = (char*)malloc(strlen(string) + 1);
	int resultIndex = 1;

	result[0] = string[0];
	for (int i = 1; i < strlen(string); i++) {
		if (!(isspace(string[i]) && isspace(string[i - 1]))) {
			result[resultIndex] = string[i];
			resultIndex++;
		}
	}
	result[resultIndex] = '\0';
	strcpy(string, result);
	free(result);
}

void deleteOddWords(char string[]) {
	strcat(string, " ");
	int count = 0;
	for (int i = 0; i < strlen(string); i++) {
		if (isspace(string[i]) || ispunct(string[i])) {
			if (count % 2 != 0) {
				for (int j = i - count; j < i; j++) {
					string[j] = ' ';
				}
			}
			count = 0;
		}
		else {
			count++;
		}
	}
	deleteSpaces(string);
}



int main() {

	char str[100];
	gets_s(str);

	deleteOddWords(str);
	puts(str);

	return 0;
}