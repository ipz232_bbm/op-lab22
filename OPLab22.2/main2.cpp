#pragma warning(disable : 4996)
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <windows.h>

int arrOfWords(char str[], char words[][20]) {
    char *token = strtok(str, " ");
    int n = 0;
    for (int i = 0; i < strlen(str); i++) {
        while (token != NULL) {
            strcpy(words[n], token);
            n++;

            token = strtok(NULL, " ");
        }
    }
    return n;
}

int isPalindrom(char words[][20], int n) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            if (i == j) continue;
            if (strlen(words[i]) == strlen(words[j])) {
                if (strcmp(words[i], strrev(words[j])) == 0) {
                    strrev(words[j]);
                    printf("\n��������� �\n");
                    return 1;
                }
                strrev(words[j]);
            }
        }
    }
    printf("\n��������� ����\n");
    return 0;
}

void repeat(char words[][20], int n) {
    int *count = (int*) calloc(n, sizeof(int));
    memset(count, 0, sizeof(count));

    for (int i = 0; i < n; i++) {
        if (count[i] == -1) {
            continue; 
        }

        for (int j = i + 1; j < n; j++) {
            if (strcmp(words[i], words[j]) == 0) {
                count[i]++;
                count[j] = -1;
            }
        }
    }
    for (int i = 0; i < n; i++) {
        if (count[i] != -1) {
            printf("\n%s ������� %d ����", words[i], count[i] + 1);
        }
    }

    int maxCountIndex = 0;
    for (int i = 1; i < n; i++) {
        if (count[i] > count[maxCountIndex]) {
            maxCountIndex = i;
        }
    }
    printf("\n����� � ��������� ������� ���������: %s (%d ����)\n", words[maxCountIndex], count[maxCountIndex] + 1);

}

int removeDuplicates(char words[][20], int n) {
    for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            if (strcmp(words[i], words[j]) == 0) {
                strcpy(words[j], "");
            }
        }
    }

    int newSize = 0;
    for (int i = 0; i < n; i++) {
        if (strcmp(words[i], "") != 0) {
            strcpy(words[newSize++], words[i]);
        }
    }
    return newSize;
}

void sort(char worlds[][20], int n) {
    int fl; char sl[20];
    do {
        fl = 0;
        for (int i = 1; i < n; i++)
            if (strcmp(worlds[i - 1], worlds[i]) > 0) {
                strcpy(sl, worlds[i - 1]);
                strcpy(worlds[i - 1], worlds[i]);
                strcpy(worlds[i], sl);
                fl = 1;
            }
    } while (fl);
}

void printWords(char words[][20], int n) {
    printf("\n");
    for (int i = 0; i < n; i++) printf("%s ", words[i]);
    printf("\n");
}

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    char str[100];
    char words[30][20];
    int wordN;
    gets_s(str);


    wordN = arrOfWords(str, words);
 
    isPalindrom(words, wordN);
    
    repeat(words, wordN);
    wordN = removeDuplicates(words, wordN);

    printf("\n������ ��� �������:");
    printWords(words, wordN);

    sort(words, wordN);

    printf("\n³����������� ������:");
    printWords(words, wordN);

    return 0;
}
